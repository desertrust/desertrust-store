class UserAbility
  include CanCan::Ability

  def initialize user
    unless user.login.blank?
      can :see, :store
    end
  end
end
