class AddFieldsToSpreeUserDRust < ActiveRecord::Migration[5.0]
  def change
    add_column :spree_users, :admin_confirmed, :boolean, default: false
  end
end
